#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

// global consts
const int g_dim = 2;
int g_refCount = 8; // number of grid refinements
size_t g_numThreads = 1;

// global typedefs
// grid
typedef Dune::YaspGrid<g_dim>                      Grid;
typedef Grid::LeafGridView                         GridView;
// basis
typedef Dune::Functions::LagrangeBasis<GridView,1> Basis;
typedef Basis::LocalView                           LocalView;
typedef LocalView::Element                         Element;
// data structures
typedef Dune::BCRSMatrix<double>                   Matrix;
typedef Dune::BlockVector<double>                  Vector;

// global system params
const auto g_volumeTerm = [](const Dune::FieldVector<double, g_dim>& x){return -5.0;};

// global inline funcs
inline void
parse_argv(int argc, char** argv)
{
    if (argc > 1) g_refCount = atoi(argv[1]);
    if (argc > 2) g_numThreads = atoi(argv[2]);
}

#endif