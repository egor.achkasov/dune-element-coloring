#ifndef BENCHMARKING_H_
#define BENCHMARKING_H_

#include <chrono>   // for std::chrono
#include <string>   // for std::string
#include <iostream> // for std::cout, std::endl

using CHRONO_CLOCK = std::chrono::high_resolution_clock;
#define CHRONO_DURCAST_MILS std::chrono::duration_cast<std::chrono::milliseconds>
static CHRONO_CLOCK::time_point g_chrono_tp1;
static int64_t g_chrono_dur_mils;

static std::string g_chrono_title = "";
void tstart(const std::string& title)
{
  g_chrono_title = title;
  g_chrono_tp1 = CHRONO_CLOCK::now();
}
void tstop()
{
  g_chrono_dur_mils = CHRONO_DURCAST_MILS(CHRONO_CLOCK::now() - g_chrono_tp1).count();
  std::cout << g_chrono_title << ": " << g_chrono_dur_mils << " ms" << std::endl;
}

#endif
