// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "globals.hh"
#include "benchmarking.hh"

#include <iostream>   // for std::cout
#include <vector>     // for std::vector

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/functions/functionspacebases/interpolate.hh>

// System assembling functions
//=========================================================================

Dune::Matrix<double>
assembleElementStiffnessMatrix(
  const LocalView& localView)
{
  using Element = typename LocalView::Element;
  constexpr int dim = Element::dimension;
  auto element = localView.element();
  auto geometry = element.geometry();
  const auto& localFiniteElement = localView.tree().finiteElement();
  Dune::Matrix<double> elementMatrix;
  elementMatrix.setSize(localView.size(),localView.size());
  elementMatrix = 0;      // Fill the entire matrix with zeros
  int order = 2 * (localFiniteElement.localBasis().order()-1);
  const auto& quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(),
                                                            order);
  for (const auto& quadPoint : quadRule)
  {
    const auto quadPos = quadPoint.position();
    const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
    const auto integrationElement = geometry.integrationElement(quadPos);
    std::vector<Dune::FieldMatrix<double,1,dim> > referenceGradients;
    localFiniteElement.localBasis().evaluateJacobian(quadPos,
                                                     referenceGradients);
    std::vector<Dune::FieldVector<double,dim> > gradients(referenceGradients.size());
    for (size_t i=0; i<gradients.size(); i++)
      jacobian.mv(referenceGradients[i][0], gradients[i]);
    for (size_t p=0; p<elementMatrix.N(); p++)
    {
      auto localRow = localView.tree().localIndex(p);
      for (size_t q=0; q<elementMatrix.M(); q++)
      {
        auto localCol = localView.tree().localIndex(q);
        elementMatrix[localRow][localCol] += (gradients[p] * gradients[q])
                                    * quadPoint.weight() * integrationElement;
      }
    }
  }

  return elementMatrix;
}

Dune::BlockVector<double>
assembleElementVolumeTerm(
        const LocalView& localView,
        const std::function<double(
          Dune::FieldVector<
            double,
            LocalView::Element::dimension
          >
        )> volumeTerm)
{
  using Element = typename LocalView::Element;
  auto element = localView.element();
  constexpr int dim = Element::dimension;
  const auto& localFiniteElement = localView.tree().finiteElement();
  Dune::BlockVector<double> localB;
  localB.resize(localFiniteElement.size());
  localB = 0;
  int order = dim;
  const auto& quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
  for (const auto& quadPoint : quadRule)
  {
    const Dune::FieldVector<double,dim>& quadPos = quadPoint.position();
    const double integrationElement = element.geometry().integrationElement(quadPos);
    double functionValue = volumeTerm(element.geometry().global(quadPos));
    std::vector<Dune::FieldVector<double,1> > shapeFunctionValues;
    localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
    for (size_t p=0; p<localB.size(); p++)
    {
      auto localIndex = localView.tree().localIndex(p);
      localB[localIndex] += shapeFunctionValues[p] * functionValue
                          * quadPoint.weight() * integrationElement;
    }
  }
  return localB;
}

//=========================================================================
// done System assembling functions

int main(int argc, char** argv)
{
  parse_argv(argc, argv);
  Dune::MPIHelper::instance(argc,argv);

  //-----------------------------------------------------------------------
  // Poisson Equation problem:
  // - \Delta u = -5 on (0,1)^2 \ [0.5,1)^2 with
  //   u = 0   on {0}x[0,1] \cup [0,1]x{0}
  //   u = 0.5 on {0.5}x[0.5,1] \cup [0.5,1]x{0.5}
  //   u' = 0  otherwise
  //-----------------------------------------------------------------------

  // init grid
  Dune::FieldVector<double,g_dim> len; for (auto& l : len) l=1.0;
  std::array<int,g_dim> cells; for (auto& c : cells) c=4;
  Grid grid(len,cells);
  grid.globalRefine(g_refCount);
  GridView gv = grid.leafGridView();
  const GridView::IndexSet& indexSet = gv.indexSet();

  // init Stiffness Matrix and the rhs vector
  Matrix stiffnessMatrix;
  Vector b;

  // assemble the system
  Basis basis(gv);
  auto localView = basis.localView();
  // assemblePoissonProblem
  //---------------------------------------------------------------------------

  // getOccupationPattern
  tstart("Get occupation pattern");
  Dune::MatrixIndexSet occupationPattern;
  occupationPattern.resize(basis.size(), basis.size());
  for (const auto& element : elements(gv)){
    localView.bind(element);
    for (size_t i=0; i<localView.size(); i++)
      for (size_t j=0; j<localView.size(); j++ )
        occupationPattern.add(localView.index(i), localView.index(j));
  }
  occupationPattern.exportIdx(stiffnessMatrix);
  tstop();

  tstart("System assembling");
  stiffnessMatrix = 0;
  b.resize(basis.dimension());
  b = 0;

  // assemble the global stiffness matrix
  for (const auto& element : elements(gv)){
    localView.bind(element);
    auto elem_matr = assembleElementStiffnessMatrix(localView);
    for(size_t p=0; p<elem_matr.N(); p++)
      for (size_t q=0; q<elem_matr.M(); q++ )
        stiffnessMatrix[localView.index(p)][localView.index(q)] += elem_matr[p][q];
  }

  // assemble the global volume term
  for (const auto& element : elements(gv)){
    localView.bind(element);
    auto localB_ptr = assembleElementVolumeTerm(localView, g_volumeTerm);
    for (size_t p=0; p<localB_ptr.size(); p++)
      b[localView.index(p)] += localB_ptr[p];
  }
  //---------------------------------------------------------------------------
  // done assemblePoissonProblem

  auto predicate = [](auto x)
  {
    return x[0] < 1e-8
        || x[1] < 1e-8
        || (x[0] > 0.4999 && x[1] > 0.4999);
  };
  std::vector<bool> dirichletNodes;
  Dune::Functions::interpolate(basis, dirichletNodes, predicate);

  // init Dirichlet values
  for (size_t i=0; i<stiffnessMatrix.N(); i++)
  {
    if (dirichletNodes[i])
    {
      auto cIt    = stiffnessMatrix[i].begin();
      auto cEndIt = stiffnessMatrix[i].end();
      for (; cIt!=cEndIt; ++cIt)
        *cIt = (cIt.index()==i) ? 1.0 : 0.0;
    }
  }
  auto dirichletValues = [](auto x)
  { return (x[0]< 1e-8 || x[1] < 1e-8) ? 0 : 0.5; };
  Dune::Functions::interpolate(basis, b, dirichletValues, dirichletNodes);
  tstop(); // "System assembling"

  /*
  // init normal solution
  Vector x_normal(basis.size());
  x_normal = b;
  Dune::MatrixAdapter<Matrix,Vector,Vector> linearOperator_normal(stiffnessMatrix);
  Dune::SeqILU<Matrix,Vector,Vector> preconditioner_normal(stiffnessMatrix, 1.0);
  Dune::CGSolver<Vector> cg_normal(linearOperator_normal, preconditioner_normal, 1e-5, 50, 0);
  Dune::InverseOperatorResult statistics_normal;

  // Perform normal FEM evaluation
  tstart("CG apply");
  cg_normal.apply(x_normal, b, statistics_normal);
  tstop();
  */

  return 0;
}
