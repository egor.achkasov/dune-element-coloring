// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "globals.hh"
#include "benchmarking.hh"

#include <iostream>   // for std::cout
#include <vector>     // for std::vector
#include <chrono>     // for std::chrono::[high_resolution_clock, duration]
#include <set>        // for std::set
#include <unordered_map>  // for std::unordered_map
#include <list>       // for std::list
#include <future>     // for std::future, std::async

#include <dune/grid/yaspgrid.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/interpolate.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>


// System assembling functions
//=========================================================================

Dune::Matrix<double>
assembleElementStiffnessMatrix(
  const LocalView& localView)
{
  using Element = typename LocalView::Element;
  constexpr int dim = Element::dimension;
  auto element = localView.element();
  auto geometry = element.geometry();
  const auto& localFiniteElement = localView.tree().finiteElement();
  Dune::Matrix<double> elementMatrix;
  elementMatrix.setSize(localView.size(),localView.size());
  elementMatrix = 0;      // Fill the entire matrix with zeros
  int order = 2 * (localFiniteElement.localBasis().order()-1);
  const auto& quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(),
                                                            order);
  for (const auto& quadPoint : quadRule)
  {
    const auto quadPos = quadPoint.position();
    const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
    const auto integrationElement = geometry.integrationElement(quadPos);
    std::vector<Dune::FieldMatrix<double,1,dim> > referenceGradients;
    localFiniteElement.localBasis().evaluateJacobian(quadPos,
                                                     referenceGradients);
    std::vector<Dune::FieldVector<double,dim> > gradients(referenceGradients.size());
    for (size_t i=0; i<gradients.size(); i++)
      jacobian.mv(referenceGradients[i][0], gradients[i]);
    for (size_t p=0; p<elementMatrix.N(); p++)
    {
      auto localRow = localView.tree().localIndex(p);
      for (size_t q=0; q<elementMatrix.M(); q++)
      {
        auto localCol = localView.tree().localIndex(q);
        elementMatrix[localRow][localCol] += (gradients[p] * gradients[q])
                                    * quadPoint.weight() * integrationElement;
      }
    }
  }

  return elementMatrix;
}

Dune::BlockVector<double>
assembleElementVolumeTerm(
        const LocalView& localView,
        const std::function<double(
          Dune::FieldVector<double, LocalView::Element::dimension>
        )> volumeTerm)
{
  using Element = typename LocalView::Element;
  auto element = localView.element();
  constexpr int dim = Element::dimension;
  const auto& localFiniteElement = localView.tree().finiteElement();
  Dune::BlockVector<double> localB;
  localB.resize(localFiniteElement.size());
  localB = 0;
  int order = dim;
  const auto& quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
  for (const auto& quadPoint : quadRule)
  {
    const Dune::FieldVector<double,dim>& quadPos = quadPoint.position();
    const double integrationElement = element.geometry().integrationElement(quadPos);
    double functionValue = volumeTerm(element.geometry().global(quadPos));
    std::vector<Dune::FieldVector<double,1> > shapeFunctionValues;
    localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
    for (size_t p=0; p<localB.size(); p++)
    {
      auto localIndex = localView.tree().localIndex(p);
      localB[localIndex] += shapeFunctionValues[p] * functionValue
                          * quadPoint.weight() * integrationElement;
    }
  }
  return localB;
}

//=========================================================================
// done System assembling functions

// Parallel workers jobs
//=========================================================================

/// Assemble a stiffness matrix from the given elements
/// \param[in]  elemsSeeds  List of element seeds to use
/// \param[in]  localView   LocalView on the grid elements
/// \param[out] stiffnessMatrix Striffness matrix object
void
thread_stiffness_matrix(
  const std::list<Element::EntitySeed>& elemSeeds,
  const Basis& basis,
  Matrix& stiffnessMatrix)
{
  auto localView = basis.localView();
  for (const auto& elem_seed : elemSeeds){
    const Element element = basis.gridView().grid().entity(elem_seed);
    localView.bind(element);
    auto elem_matr = assembleElementStiffnessMatrix(localView);
    for(size_t p=0; p<elem_matr.N(); p++)
      for (size_t q=0; q<elem_matr.M(); q++)
        stiffnessMatrix[localView.index(p)][localView.index(q)] += elem_matr[p][q];
  }
}

/// Assemble a volume term from the given elements
/// \param[in]  elems     List of element indices to use
/// \param[in]  localView LocalView on the grid elements
/// \param[out] b         Volume term vector object
void
thread_b(
  const std::list<Element::EntitySeed>& elemSeeds,
  const Basis& basis,
  Vector& b)
{
  auto localView = basis.localView();
  for (const auto& elem_seed : elemSeeds){
    const Element element = basis.gridView().grid().entity(elem_seed);
    localView.bind(element);
    auto elem_term = assembleElementVolumeTerm(localView, g_volumeTerm);
    for(size_t p=0; p<elem_term.N(); p++)
        b[localView.index(p)] += elem_term[p];
  }
}

//=========================================================================
// done Parallel workers jobs

int main(int argc, char** argv)
{
  parse_argv(argc, argv);
  Dune::MPIHelper::instance(argc,argv);

  //-----------------------------------------------------------------------
  // Poisson Equation problem:
  // - \Delta u = -5 on (0,1)^2 \ [0.5,1)^2 with
  //   u = 0   on {0}x[0,1] \cup [0,1]x{0}
  //   u = 0.5 on {0.5}x[0.5,1] \cup [0.5,1]x{0.5}
  //   u' = 0  otherwise
  //-----------------------------------------------------------------------

  // init grid
  Dune::FieldVector<double,g_dim> len; for (auto& l : len) l=1.0;
  std::array<int,g_dim> cells; for (auto& c : cells) c=4;
  Grid grid(len,cells);
  grid.globalRefine(g_refCount);
  GridView gv = grid.leafGridView();
  const GridView::IndexSet& indexSet = gv.indexSet();

  // init Stiffness Matrix and the rhs vector
  Matrix stiffnessMatrix;
  Vector b;

  // assemble the system
  Basis basis(gv);
  auto localView = basis.localView();
  // assemblePoissonProblem
  //---------------------------------------------------------------------------

  // getOccupationPattern
  //===========================================================================
  Dune::MatrixIndexSet occupationPattern;
  occupationPattern.resize(basis.size(), basis.size());
  for (const auto& element : elements(gv)){
    localView.bind(element);
    for (size_t i=0; i<localView.size(); i++)
      for (size_t j=0; j<localView.size(); j++ )
        occupationPattern.add(localView.index(i), localView.index(j));
  }
  //===========================================================================
  // done getOccupationPattern
  occupationPattern.exportIdx(stiffnessMatrix);

  // color elements
  tstart("Elements coloring");
  // {Element index -> color} hashmap
  std::vector< std::list<Element::EntitySeed> > clrs_seeds;
  // Global DOF global id to a set of colors of adjacent elements
  std::map<size_t, std::set<size_t> > dofGid2elemClrs;
  std::list<size_t> colors;
  // for each element
  for (const Element& element : elements(gv)){
    localView.bind(element);
    // Get adjacent elements colors
    std::set<size_t> adj_clrs;
    for (size_t dof=0; dof != localView.size(); ++dof){
      size_t dofgid = localView.index(dof);
      if (!dofGid2elemClrs.count(dofgid)) continue;
      std::set<size_t>* adj_dof_clrs = &dofGid2elemClrs.at(dofgid);
      adj_clrs.insert(adj_dof_clrs->begin(), adj_dof_clrs->end());
    }
    // Pick the element color
    size_t elem_clr = 0;
    while (adj_clrs.count(elem_clr)) ++elem_clr;
    // Color the element
    if (elem_clr >= clrs_seeds.size())
      for (size_t i=elem_clr-clrs_seeds.size()+1; i != 0; --i)
        clrs_seeds.push_back(std::list<Element::EntitySeed>());
    clrs_seeds[elem_clr].push_back(element.seed());
    for (size_t dof=0; dof != localView.size(); ++dof){
      size_t dofgid = localView.index(dof);
      auto it = dofGid2elemClrs.find(dofgid);
      if (it == dofGid2elemClrs.end())
        dofGid2elemClrs.insert({dofgid, {elem_clr}});
      else
        it->second.insert(elem_clr);
    }
    colors.push_back(elem_clr);
  }
  tstop();

  // Write grid with VTK
  //===========================================================================

  std::vector<size_t> colors_v;
  colors_v.reserve(colors.size());
  for (size_t c : colors)
    colors_v.push_back(c);

  Dune::VTKWriter<GridView> vtkWriter(gv);
  vtkWriter.addCellData(colors_v, "Colors");
  vtkWriter.write("getting-started-poisson-fem-result");

  //===========================================================================
  // done Write grid with VTK

  return 0;
}
