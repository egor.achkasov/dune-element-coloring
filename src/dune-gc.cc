// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include "globals.hh"
#include "benchmarking.hh"

#include <dune/grid/yaspgrid.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/interpolate.hh>

#include <iostream>   // for std::cout
#include <vector>     // for std::vector
#include <chrono>     // for std::chrono::[high_resolution_clock, duration]
#include <set>        // for std::set
#include <unordered_map>  // for std::unordered_map
#include <list>       // for std::list
#include <thread>     // for std::thread
#include <queue>      // for std::queue


// System assembling functions
//=========================================================================

Dune::Matrix<double>
assembleElementStiffnessMatrix(
  const LocalView& localView)
{
  using Element = typename LocalView::Element;
  constexpr int dim = Element::dimension;
  auto element = localView.element();
  auto geometry = element.geometry();
  const auto& localFiniteElement = localView.tree().finiteElement();
  Dune::Matrix<double> elementMatrix;
  elementMatrix.setSize(localView.size(),localView.size());
  elementMatrix = 0;      // Fill the entire matrix with zeros
  int order = 2 * (localFiniteElement.localBasis().order()-1);
  const auto& quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(),
                                                            order);
  for (const auto& quadPoint : quadRule)
  {
    const auto quadPos = quadPoint.position();
    const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
    const auto integrationElement = geometry.integrationElement(quadPos);
    std::vector<Dune::FieldMatrix<double,1,dim> > referenceGradients;
    localFiniteElement.localBasis().evaluateJacobian(quadPos,
                                                     referenceGradients);
    std::vector<Dune::FieldVector<double,dim> > gradients(referenceGradients.size());
    for (size_t i=0; i<gradients.size(); i++)
      jacobian.mv(referenceGradients[i][0], gradients[i]);
    for (size_t p=0; p<elementMatrix.N(); p++)
    {
      auto localRow = localView.tree().localIndex(p);
      for (size_t q=0; q<elementMatrix.M(); q++)
      {
        auto localCol = localView.tree().localIndex(q);
        elementMatrix[localRow][localCol] += (gradients[p] * gradients[q])
                                    * quadPoint.weight() * integrationElement;
      }
    }
  }

  return elementMatrix;
}

Dune::BlockVector<double>
assembleElementVolumeTerm(
        const LocalView& localView,
        const std::function<double(
          Dune::FieldVector<double, LocalView::Element::dimension>
        )> volumeTerm)
{
  using Element = typename LocalView::Element;
  auto element = localView.element();
  constexpr int dim = Element::dimension;
  const auto& localFiniteElement = localView.tree().finiteElement();
  Dune::BlockVector<double> localB;
  localB.resize(localFiniteElement.size());
  localB = 0;
  int order = dim;
  const auto& quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
  for (const auto& quadPoint : quadRule)
  {
    const Dune::FieldVector<double,dim>& quadPos = quadPoint.position();
    const double integrationElement = element.geometry().integrationElement(quadPos);
    double functionValue = volumeTerm(element.geometry().global(quadPos));
    std::vector<Dune::FieldVector<double,1> > shapeFunctionValues;
    localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
    for (size_t p=0; p<localB.size(); p++)
    {
      auto localIndex = localView.tree().localIndex(p);
      localB[localIndex] += shapeFunctionValues[p] * functionValue
                          * quadPoint.weight() * integrationElement;
    }
  }
  return localB;
}

//=========================================================================
// done System assembling functions

// Parallel workers jobs
//=========================================================================

/// Assemble a stiffness matrix and a volume term for the given elements
/// \param[in]  cit        Constant iterator to the element in a list to start assembly from
/// \param[in]  cend       cend of the list
/// \param[in]  num_elems  Amount of elemnts to proccess
/// \param[in]  basis      Basis of the system
/// \param[out] stiffnessMatrix Striffness matrix object
/// \param[out] b          Volume term vector object
void
thread_elem(
  std::list<Element::EntitySeed>::const_iterator cit,
  std::list<Element::EntitySeed>::const_iterator cend,
  size_t num_elems,
  const Basis& basis,
  Matrix& stiffnessMatrix,
  Vector& b)
{
  auto localView = basis.localView();
  for (size_t num_done = 0;
       cit != cend && num_done != num_elems;
       ++num_done, ++cit)
  {
    const Element element = basis.gridView().grid().entity(*cit);
    localView.bind(element);
  
    // lhs
    auto elem_matr = assembleElementStiffnessMatrix(localView);
    for(size_t p=0; p<elem_matr.N(); p++)
      for (size_t q=0; q<elem_matr.M(); q++)
        stiffnessMatrix[localView.index(p)][localView.index(q)] += elem_matr[p][q];
    // rhs
    auto elem_term = assembleElementVolumeTerm(localView, g_volumeTerm);
    for(size_t p=0; p<elem_term.N(); p++)
        b[localView.index(p)] += elem_term[p];
  }
}

//=========================================================================
// done Parallel workers jobs

int main(int argc, char** argv)
{
  parse_argv(argc, argv);
  Dune::MPIHelper::instance(argc,argv);

  //-----------------------------------------------------------------------
  // Poisson Equation problem:
  // - \Delta u = -5 on (0,1)^2 \ [0.5,1)^2 with
  //   u = 0   on {0}x[0,1] \cup [0,1]x{0}
  //   u = 0.5 on {0.5}x[0.5,1] \cup [0.5,1]x{0.5}
  //   u' = 0  otherwise
  //-----------------------------------------------------------------------

  // init grid
  Dune::FieldVector<double,g_dim> len; for (auto& l : len) l=1.0;
  std::array<int,g_dim> cells; for (auto& c : cells) c=4;
  Grid grid(len,cells);
  grid.globalRefine(g_refCount);
  GridView gv = grid.leafGridView();
  const GridView::IndexSet& indexSet = gv.indexSet();

  // init Stiffness Matrix and the rhs vector
  Matrix stiffnessMatrix;
  Vector b;

  // assemble the system
  Basis basis(gv);
  auto localView = basis.localView();
  // assemblePoissonProblem
  //---------------------------------------------------------------------------

  // getOccupationPattern
  tstart("Get occupation pattern");
  Dune::MatrixIndexSet occupationPattern;
  occupationPattern.resize(basis.size(), basis.size());
  for (const auto& element : elements(gv)){
    localView.bind(element);
    for (size_t i=0; i<localView.size(); i++)
      for (size_t j=0; j<localView.size(); j++ )
        occupationPattern.add(localView.index(i), localView.index(j));
  }
  occupationPattern.exportIdx(stiffnessMatrix);
  tstop();

  // color elements
  tstart("Elements coloring");
  // {Element index -> color} hashmap
  std::vector< std::list<Element::EntitySeed> > clrs_seeds;
  // Global DOF global id to a set of colors of adjacent elements
  std::map<size_t, std::set<size_t> > dofGid2elemClrs;
  // for each element
  for (const Element& element : elements(gv)){
    localView.bind(element);
    // Get adjacent elements colors
    std::set<size_t> adj_clrs;
    for (size_t dof=0; dof != localView.size(); ++dof){
      size_t dofgid = localView.index(dof);
      if (!dofGid2elemClrs.count(dofgid)) continue;
      std::set<size_t>* adj_dof_clrs = &dofGid2elemClrs.at(dofgid);
      adj_clrs.insert(adj_dof_clrs->begin(), adj_dof_clrs->end());
    }
    // Pick the element color
    size_t elem_clr = 0;
    while (adj_clrs.count(elem_clr)) ++elem_clr;
    // Color the element
    if (elem_clr >= clrs_seeds.size())
      for (size_t i=elem_clr-clrs_seeds.size()+1; i != 0; --i)
        clrs_seeds.push_back(std::list<Element::EntitySeed>());
    clrs_seeds[elem_clr].push_back(element.seed());
    for (size_t dof=0; dof != localView.size(); ++dof){
      size_t dofgid = localView.index(dof);
      auto it = dofGid2elemClrs.find(dofgid);
      if (it == dofGid2elemClrs.end())
        dofGid2elemClrs.insert({dofgid, {elem_clr}});
      else
        it->second.insert(elem_clr);
    }
  }
  tstop(); // "DOF coloring"

  //===========================================================================
  // done color dofs

  // assemblePoissonProblem
  //---------------------------------------------------------------------------
  tstart("System assembling");

  stiffnessMatrix = 0;
  b.resize(basis.dimension());
  b = 0;

  // assemble the global stiffness matrices and the volume terms for each color
  std::queue<std::thread> thds;  // stiffness matrix and  volume term assemblers
  for (const std::list<Element::EntitySeed>& elemSeeds : clrs_seeds) {
    typedef std::list<Element::EntitySeed>::const_iterator elemSeedsConstIt;
    size_t elems_per_thd = elemSeeds.size() / g_numThreads;
    if (elemSeeds.size() % g_numThreads) ++elems_per_thd;
    elemSeedsConstIt cend = elemSeeds.cend();

    // Run the jobs
    for (elemSeedsConstIt cit = elemSeeds.cbegin(); cit != cend;)
    {
      thds.push(std::thread(
        thread_elem,
        cit,
        cend,
        elems_per_thd,
        std::cref(basis),
        std::ref(stiffnessMatrix),
        std::ref(b)
      ));
      for (size_t i = 0; i != elems_per_thd && cit != cend; ++i)
        ++cit;
    }
    while (!thds.empty()) {
      thds.front().join();
      thds.pop();
    }
  }

  //---------------------------------------------------------------------------
  // done assemblePoissonProblem

  auto predicate = [](auto x)
  {
    return x[0] < 1e-8
        || x[1] < 1e-8
        || (x[0] > 0.4999 && x[1] > 0.4999);
  };
  std::vector<bool> dirichletNodes;
  Dune::Functions::interpolate(basis, dirichletNodes, predicate);

  // init Dirichlet values
  auto dirichletValues = [](auto x)
  { return (x[0]< 1e-8 || x[1] < 1e-8) ? 0 : 0.5; };
  Dune::Functions::interpolate(basis, b, dirichletValues, dirichletNodes);
  for (size_t i=0; i<stiffnessMatrix.N(); i++)
  {
    if (dirichletNodes[i])
    {
      auto cIt    = stiffnessMatrix[i].begin();
      auto cEndIt = stiffnessMatrix[i].end();
      for (; cIt!=cEndIt; ++cIt)
        *cIt = (cIt.index()==i) ? 1.0 : 0.0;
    }
  }
  tstop(); // "System assembling"

  /*
  // init normal solution
  Vector x_normal(basis.size());
  x_normal = b;
  Dune::MatrixAdapter<Matrix,Vector,Vector> linearOperator_normal(stiffnessMatrix);
  Dune::SeqILU<Matrix,Vector,Vector> preconditioner_normal(stiffnessMatrix, 1.0);
  Dune::CGSolver<Vector> cg_normal(linearOperator_normal, preconditioner_normal, 1e-5, 50, 0);
  Dune::InverseOperatorResult statistics_normal;

  // Perform FEM evaluation
  tstart("CG apply");
  cg_normal.apply(x_normal, b, statistics_normal);
  tstop();
  */

  return 0;
}
