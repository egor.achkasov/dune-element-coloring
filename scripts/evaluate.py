"""Script for running the programs and storing output.
Usage:
    python evaluate.py num_tries refCount_start refCount_stop max_num_threads

Example:
    python evaluate.py 10 1 10 18

Stores the results into a .npz archive with filename \"evaluation-*unixtime*.npz\".
The archive contains 3 np.ndarrays:
    ms_nogc_gop (refCount_stop-refCount_start, num_tries):
        getOccupationPattern times in dune_nogc
    ms_nogc_asm (refCount_stop-refCount_start, num_tries):
        Assembling times in dune_nogc
    ms_gc_gop (refCount_stop-refCount_start, num_thds, num_tries):
        getOccupationPattern times in dune_gc
    ms_gc_gc (refCount_stop-refCount_start, num_thds, num_tries):
        Coloring times in dune_gc
    ms_gc_asm (refCount_stop-refCount_start, num_thds, num_tries):
        Assembling times in dune_gc
"""

import numpy as np
from subprocess import Popen, PIPE
from sys import argv, stdout
from time import time
from itertools import product

# Number of runs of a program per parameter set
g_num_tries = int(argv[1])
# Refinement count range
g_refCount_start = int(argv[2])
g_refCount_stop = int(argv[3])
g_refCount_num = g_refCount_stop - g_refCount_start
# Maximum number of threads
g_num_thds_max = int(argv[4])
# Path to the directory with the executables that will be run
g_bin_dir_path = __file__[:__file__.rfind("/")] + "/../build-cmake/src"
# Evaluation times
g_ms_nogc_gop = np.empty((g_refCount_num, g_num_tries), int)
g_ms_nogc_asm = np.empty((g_refCount_num, g_num_tries), int)
g_ms_gc_gop = np.empty((g_refCount_num, g_num_thds_max, g_num_tries), int)
g_ms_gc_asm = np.empty((g_refCount_num, g_num_thds_max, g_num_tries), int)
g_ms_gc_gc =  np.empty((g_refCount_num, g_num_thds_max, g_num_tries), int)

def parse_line_ms(line: str) -> int:
    """Get ms count from a line in format \"%s: %d ms\""""
    pos = line[:-3].rfind(": ")
    return int(line[pos+2:-3])

# init run argument ranges
g_refCount_range = range(g_refCount_start, g_refCount_stop)
g_num_tries_range = range(g_num_tries)
g_num_thds_range = range(1, g_num_thds_max + 1)

# test nogc (no coloring)
bin_path = g_bin_dir_path + "/dune-nogc"
for refCount, try_i in product(g_refCount_range, g_num_tries_range):
    stdout.write(f"Running dune_nogc {refCount} (try {try_i}/{g_num_tries})...\r")
    stdout.flush()
    # run the program
    process = Popen([bin_path, str(refCount)], stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()
    if exit_code != 0:
        exit(exit_code)
    # parse the output
    lines = str(output).split("\\n")[:-1]
    refCount_i = refCount - g_refCount_start
    g_ms_nogc_gop[refCount_i, try_i] = parse_line_ms(lines[0])
    g_ms_nogc_asm[refCount_i, try_i] = parse_line_ms(lines[1])

# test gc (with coloring and parallel assembly)
bin_path = g_bin_dir_path + "/dune-gc"
for refCount, num_thds, try_i in product(g_refCount_range, g_num_thds_range, g_num_tries_range):
    stdout.write(f"Running dune_gc {refCount} {num_thds} (try {try_i}/{g_num_tries})...\r")
    stdout.flush()
    # run the program
    process = Popen([bin_path, str(refCount), str(num_thds)], stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()
    if exit_code != 0:
        exit(exit_code)
    # parse the output
    lines = str(output).split("\\n")[:-1]
    refCount_i = refCount - g_refCount_start
    g_ms_gc_gop[refCount_i, num_thds - 1, try_i] = parse_line_ms(lines[0])
    g_ms_gc_gc [refCount_i, num_thds - 1, try_i] = parse_line_ms(lines[1])
    g_ms_gc_asm[refCount_i, num_thds - 1, try_i] = parse_line_ms(lines[2])

# Save the results to a file
filename = "evaluation-" + str(int(time()))
np.savez(filename,
         g_ms_nogc_gop, g_ms_nogc_asm,
         g_ms_gc_gop, g_ms_gc_gc, g_ms_gc_asm)

